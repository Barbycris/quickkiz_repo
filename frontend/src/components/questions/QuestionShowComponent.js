import React, { Component }from 'react';

import { Link } from 'react-router-dom';

import Paper from '@material-ui/core/Paper';
import Button from '@material-ui/core/Button';
import Typography from '@material-ui/core/Typography';

import Table from '@material-ui/core/Table';
import TableBody from '@material-ui/core/TableBody';
import TableCell from '@material-ui/core/TableCell';
import TableHead from '@material-ui/core/TableHead';
import TableRow from '@material-ui/core/TableRow';


class QuestionShowComponent extends Component {
  state = {
    pool: {
      id: 1,
      topic: 'Expresiones Matemáticas',
      questions: [
        { id: 1, enunciado: 'Resultado de...', code: 'print a+b', variables: {a:1, b:2}},
        { id: 2, enunciado: 'Que muestra el print?', code: 'print a+b', variables: {a:1, b:2}},
        { id: 3, enunciado: 'Es correcta la traza?', code: 'print a+b', variables: {a:1, b:2}},
      ]
    }
  };

  render() {
    return (
      <div>
        <Typography variant="h4" gutterBottom>Pozo: { this.state.pool.topic }</Typography>
        <Paper>
          <Table>
            <TableHead>
              <TableRow>
                <TableCell>Enunciado de la pregunta</TableCell>
                <TableCell>Código</TableCell>
                <TableCell></TableCell>
                <TableCell></TableCell>
                <TableCell></TableCell>
              </TableRow>
            </TableHead>
            <TableBody>
              {this.state.pool.questions.map(question => {
                return (
                  <TableRow key={question.id}>
                    <TableCell component="th" scope="pool">
                      { question.enunciado }
                    </TableCell>
                    <TableCell>
                      <code>
                        { question.code }
                      </code>
                    </TableCell>
                    <TableCell>
                      <Button color="default" component={Link} to={"/questions/"+question.id}>
                        Ver
                      </Button>
                    </TableCell>
                    <TableCell>
                      <Button color="primary" component={Link} to={"/questions/"+question.id+"/edit"}>
                        Editar
                      </Button>
                    </TableCell>
                    <TableCell>
                      <Button color="secondary" component={Link} to={"/questions/"+question.id+"/delete"}>
                        Remover
                      </Button>
                    </TableCell>
                  </TableRow>
                );
              })}
              </TableBody>
          </Table>
        </Paper>

        <Button variant="contained" color="primary" component={Link} to="/questions/create">
          Agregar pregunta al Pozo
        </Button>
      </div>
    );
  }
}

export default QuestionShowComponent;
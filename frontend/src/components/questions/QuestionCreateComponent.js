import React, { Component }from 'react';

import { Link } from 'react-router-dom';

import TextField from '@material-ui/core/TextField';
import Button from '@material-ui/core/Button';
import Typography from '@material-ui/core/Typography';

import FilledInput from '@material-ui/core/FilledInput';
import InputLabel from '@material-ui/core/InputLabel';
import MenuItem from '@material-ui/core/MenuItem';
import FormControl from '@material-ui/core/FormControl';
import Select from '@material-ui/core/Select';
import Grid from '@material-ui/core/Grid';

class QuestionCreateComponent extends Component {
  state = {
    question: {
      pool_id: '',
      enunciado: '',
      code: '',
      variables: {},
    },
    pools: [
      { id: 1, topic: 'Expresiones Matematicas' },
      { id: 2, topic: 'Estructura de scripting' },
      { id: 3, topic: 'Expresiones booleanas' },
      { id: 4, topic: 'Decisiones' },
    ]
  };

  handleChange = event => {
      this.setState({
        [event.target.name]: event.target.value
      });
    };

  render() {
    return (
      <div>
        <Typography variant="h4" gutterBottom>Agregar Pregunta al Pozo</Typography>
        
        <Grid container spacing={24}>
          <Grid item xs={6}>
            <form>
              <FormControl variant="filled" margin="normal" fullWidth>
                <InputLabel htmlFor="filled-age-simple">Pozo</InputLabel>
                <Select
                  value={this.state.question.pool_id}
                  onChange={this.handleChange}
                  input={<FilledInput name="pool" id="filled-pool-simple" />}
                >
                  {this.state.pools.map(pool => {
                    return (
                      <MenuItem key={ pool.id } value={ pool.id }>{ pool.topic }</MenuItem>
                    )
                  })};
                </Select>
              </FormControl>

              <TextField
                        id="varaibles"
                        label="Variables"
                        placeholder="Ingrese variables del código"
                        multiline
                        fullWidth
                        margin="normal"
                        variant="filled"
                        InputLabelProps={{
                          shrink: true,
                        }}
                      />
              <TextField
                        id="enunciado"
                        label="Enunciado"
                        placeholder="Ingrese tematica del pozo"
                        multiline
                        fullWidth
                        margin="normal"
                        variant="filled"
                        InputLabelProps={{
                          shrink: true,
                        }}
                      />

              <Button component={Link} to="/pools">
                Cancelar
              </Button>
              <Button variant="contained" color="primary" type="submit">
                Crear
              </Button>
            </form>
          </Grid>
          <Grid item xs={6}>
            <Typography variant="h6" gutterBottom>Vista Previa</Typography>
          </Grid>
        </Grid>
      </div>
    );
  }
}

export default QuestionCreateComponent;
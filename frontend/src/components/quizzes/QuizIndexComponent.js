import React, { Component }from 'react';

import { Link } from 'react-router-dom';

import Table from '@material-ui/core/Table';
import TableBody from '@material-ui/core/TableBody';
import TableCell from '@material-ui/core/TableCell';
import TableHead from '@material-ui/core/TableHead';
import TableRow from '@material-ui/core/TableRow';
import Paper from '@material-ui/core/Paper';
import Button from '@material-ui/core/Button';
import Typography from '@material-ui/core/Typography';


class QuizIndexComponent extends Component {
  state = {
    quizzes: [
      {
        id: 1,
        name: 'Quiz #1',
        questions: [
          { topic_id: 'Expresiones Matematicas', points: 3 },
          { topic_id: 'Expresiones Matematicas', points: 3 },
        ]
      },
      {
        id: 2,
        name: 'Quiz #2',
        questions: [
          { topic_id: 'Estructura de scripting', points: 3 },
          { topic_id: 'Estructura de scripting', points: 3 },
        ]
      },
      {
        id: 3,
        name: 'Quiz #3',
        questions: [
          { topic_id: 'Expresiones booleanas', points: 3 },
          { topic_id: 'Expresiones booleanas', points: 3 },
        ]
      },
      {
        id: 4,
        name: 'Quiz #4',
        questions: [
          { topic_id: 'Decisiones', points: 3 },
          { topic_id: 'Decisiones', points: 3 },
        ]
      }
    ],
  };

  render() {
    return (
      <div>
        <Typography variant="h4" gutterBottom>Quizzes</Typography>
        <Paper>
          <Table>
            <TableHead>
              <TableRow>
                <TableCell>Nombre del Quiz</TableCell>
                <TableCell>Tópicos de Preguntas (Pozos)</TableCell>
                <TableCell></TableCell>
                <TableCell></TableCell>
                <TableCell></TableCell>
              </TableRow>
            </TableHead>
            <TableBody>
              {this.state.quizzes.map(quiz => {
                return (
                  <TableRow key={ quiz.id }>
                    <TableCell component="th" scope="quiz">
                      { quiz.name }
                    </TableCell>
                    <TableCell>
                      { quiz.questions.map(question => {
                        return (
                            question.topic_id + ", "
                          );
                      })}
                    </TableCell>
                    <TableCell>
                      <Button color="default" component={Link} to={"/quizzes/"+quiz.id}>
                        Ver
                      </Button>
                    </TableCell>
                    <TableCell>
                      <Button color="primary" component={Link} to={"/quizzes/"+quiz.id+"/edit"}>
                        Editar
                      </Button>
                    </TableCell>
                    <TableCell>
                      <Button color="secondary" component={Link} to={"/quizzes/"+quiz.id+"/delete"}>
                        Borrar
                      </Button>
                    </TableCell>
                  </TableRow>
                );
              })}
            </TableBody>
          </Table>
        </Paper>

        <Button variant="contained" color="primary" component={Link} to="/quizzes/create">
          Crear Nuevo Quiz
        </Button>
      </div>
    );
  }
}

export default QuizIndexComponent;
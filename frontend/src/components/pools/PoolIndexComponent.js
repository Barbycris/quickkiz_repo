import React, { Component } from 'react';

import { Link } from 'react-router-dom';
import axios from 'axios';

import Table from '@material-ui/core/Table';
import TableBody from '@material-ui/core/TableBody';
import TableCell from '@material-ui/core/TableCell';
import TableHead from '@material-ui/core/TableHead';
import TableRow from '@material-ui/core/TableRow';
import Paper from '@material-ui/core/Paper';
import Button from '@material-ui/core/Button';
import Typography from '@material-ui/core/Typography';

const URL_API = 'http://localhost:3004';

class PoolIndexComponent extends Component {
  constructor(props) {
    super(props);

    this.state = {
      pools: []
    };

    this.delete = this.delete.bind(this);
  }

  componentDidMount() {
    this.getPools();
  }

  getPools() {
    axios.get(URL_API + '/pools')
      .then(result => this.setState({
        pools: result.data,
      }))
      .catch(error => this.setState({
        pools: [],
      }));
  }

  delete(event) {
    if (window.confirm('¿Esta seguro que desea eliminar el pozo?')) {
      const id = event.target.id.value;
      
      axios.delete(URL_API + '/pools/' + id)
        .then(result => {
          this.getPools();

          console.log('pool eliminado');
          // window.M.toast({html: 'Producto Eliminado'});
        })
        .catch(error => {
            console.error(error);
        });
    }
    
    event.preventDefault();
  }

  render() {
    return (
      <div>
        <Typography variant="h4" gutterBottom>Pozos de preguntas</Typography>
        <Paper>
          <Table>
            <TableHead>
              <TableRow>
                <TableCell>Nombre del Pozo</TableCell>
                <TableCell></TableCell>
                <TableCell></TableCell>
                <TableCell></TableCell>
              </TableRow>
            </TableHead>
            <TableBody>
              {this.state.pools.map(pool => {
                return (
                  <TableRow key={ pool.id }>
                    <TableCell component="th" scope="pool">
                      { pool.name }
                    </TableCell>
                    <TableCell>
                      <Button color="default" component={Link} to={"/pools/"+pool.id}>
                        Ver
                      </Button>
                    </TableCell>
                    <TableCell>
                      <Button color="primary" component={Link} to={"/pools/"+pool.id+"/edit"}>
                        Editar
                      </Button>
                    </TableCell>
                    <TableCell>
                      <form onSubmit={this.delete}>
                        <input type="hidden" name="id" value={pool.id}/>
                        <Button color="secondary" type="submit">
                          Borrar
                        </Button>
                      </form>
                    </TableCell>
                  </TableRow>
                );
              })}
            </TableBody>
          </Table>
        </Paper>

        <Button variant="contained" color="primary" component={Link} to="/pools/create">
          Crear Nuevo Pozo
        </Button>
      </div>
    );
  }
}

export default PoolIndexComponent;
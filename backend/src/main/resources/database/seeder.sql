-- DROP IF EXIST quickiz;

-- CREATE DATABASE quickiz;

-- USE quickiz;

--
-- Seed
--
INSERT INTO users (name, email, role_id) VALUES
('Isaias Cardenas', 'isaias.cardenas@usach.cl', 1),
('Diego Salazar', 'diego.salazar.se@usach.cl', 1);

INSERT INTO roles (name, description) VALUES
('admin', 'Administrador general de la aplicación'),
('coordinador', 'Administrador de secciones y profesores'),
('profesor', 'Administrador de cursos'),
('alumno', 'Visualizador del sistema');

INSERT INTO questions (title, code, variables, pool_id, user_id) VALUES
('Pregunta uno', 'aoriesnt aoriesnt oairesnto arst', "{'key': 'value'}", 1, 2),
('Pregunta dos', 'moiearsnt  moairestna en ar', "{'foo': 'bar'}", 2, 3),
('Pregunta tres', 'moiear omoiearn stul ars', "{'key': 'value'}", 1, 1),
('Pregunta cuatro', 'ienarostmoien arsotieanr st', "{'foo': 'bar'}", 2, 1);

INSERT INTO pools (tema) VALUES
('while'),
('for'),
('if'),
('switch');


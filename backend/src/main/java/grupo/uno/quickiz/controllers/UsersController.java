package grupo.uno.quickiz.controllers;

import java.util.List;
import grupo.uno.quickiz.models.User;
import org.springframework.http.HttpStatus;
import grupo.uno.quickiz.repositories.UserRepository;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseStatus;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.beans.factory.annotation.Autowired;


@RestController
// @CrossOrigin(origins = "http://159.65.253.247:80")
public class UsersController {

    @Autowired
    private UserRepository userRepository;

    @RequestMapping(value = "/users", method = RequestMethod.GET)
    public List<User> getUser()
    {
        return userRepository.findAll();
    }

    @RequestMapping(value = "/users/{id}", method = RequestMethod.GET, produces = "application/json")
    public User getUserById(@PathVariable Integer id) {
        return userRepository.findById(id).get();
    }

    @RequestMapping(value="/users", method = RequestMethod.POST)
    @ResponseStatus(HttpStatus.CREATED)
    @ResponseBody
    public User createUser(@RequestBody User user)
    {
        return userRepository.save(user);
    }

    @RequestMapping(value="/users/{id}", method = RequestMethod.PUT)
    public User updateUser(@RequestBody User user, @PathVariable Integer id) {

        User u = userRepository.findById(id).get();
        // verify if exist

        u.setName(user.getName());
        u.setEmail(user.getEmail());
        u.setRole_id(user.getRole_id());

        userRepository.save(u);
        return u;
    }

    @RequestMapping(value = "/users/{id}", method = RequestMethod.DELETE)
    public void deleteUser(@PathVariable Integer id) {
        // verify if exist
        userRepository.deleteById(id);
    }
}


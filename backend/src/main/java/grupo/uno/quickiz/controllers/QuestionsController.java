package grupo.uno.quickiz.controllers;

import java.util.List;
import grupo.uno.quickiz.models.Question;
import org.springframework.http.HttpStatus;
import grupo.uno.quickiz.repositories.QuestionRepository;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseStatus;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.beans.factory.annotation.Autowired;


@RestController
// @CrossOrigin(origins = "http://159.65.253.247:80")
public class QuestionsController {

    @Autowired
    private QuestionRepository questionRepository;

    @RequestMapping(value = "/questions", method = RequestMethod.GET)
    public List<Question> getQuestions()
    {
        return questionRepository.findAll();
    }

    @RequestMapping(value = "/questions/{id}", method = RequestMethod.GET, produces = "application/json")
    public Question getQuestionById(@PathVariable Integer id) {
        return questionRepository.findById(id).get();
    }

    @RequestMapping(value="/questions", method = RequestMethod.POST)
    @ResponseStatus(HttpStatus.CREATED)
    @ResponseBody
    public Question createQuestion(@RequestBody Question question)
    {
        return questionRepository.save(question);
    }

    @RequestMapping(value="/questions/{id}", method = RequestMethod.PUT)
    public Question updateQuestion(@RequestBody Question question, @PathVariable Integer id) {

        Question q = questionRepository.findById(id).get();
        // verify if exist

        q.setTitle(question.getTitle());

        questionRepository.save(q);
        return q;
    }

    @RequestMapping(value = "/questions/{id}", method = RequestMethod.DELETE)
    public void deleteQuestion(@PathVariable Integer id) {
        // verify if exist
        questionRepository.deleteById(id);
    }
}


package grupo.uno.quickiz.repositories;

import grupo.uno.quickiz.models.Question;
import org.springframework.data.jpa.repository.JpaRepository;

public interface QuestionRepository extends JpaRepository<Question, Integer> {

    Question findByTitle(String title);
    void deleteByTitle(String title);

}


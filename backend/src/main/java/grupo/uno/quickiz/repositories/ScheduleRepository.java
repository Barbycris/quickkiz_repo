package grupo.uno.quickiz.repositories;

import grupo.uno.quickiz.models.Schedule;
import org.springframework.data.jpa.repository.JpaRepository;

public interface ScheduleRepository extends JpaRepository<Schedule, Integer> {

    Schedule findByCode(String code);
    void deleteByCode(String code);

}


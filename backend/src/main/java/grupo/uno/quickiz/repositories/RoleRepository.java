package grupo.uno.quickiz.repositories;

import grupo.uno.quickiz.models.Role;
import org.springframework.data.jpa.repository.JpaRepository;

public interface RoleRepository extends JpaRepository<Role, Integer> {

    Role findByName(String name);
    void deleteByName(String name);

}


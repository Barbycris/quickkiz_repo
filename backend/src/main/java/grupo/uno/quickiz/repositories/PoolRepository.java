package grupo.uno.quickiz.repositories;

import grupo.uno.quickiz.models.Pool;
import org.springframework.data.jpa.repository.JpaRepository;

public interface PoolRepository extends JpaRepository<Pool, Integer> {

    Pool findByTema(String tema);

}


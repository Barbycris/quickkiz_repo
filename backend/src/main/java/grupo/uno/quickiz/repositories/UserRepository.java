package grupo.uno.quickiz.repositories;

import grupo.uno.quickiz.models.User;
import org.springframework.data.jpa.repository.JpaRepository;

public interface UserRepository extends JpaRepository<User, Integer> {

    User findByName(String name);
    void deleteByName(String name);

}


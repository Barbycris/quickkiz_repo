package grupo.uno.quickiz.models;

import lombok.Getter;
import lombok.Setter;
import javax.persistence.Id;
import org.joda.time.DateTime;
import javax.persistence.Table;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;

@Setter
@Getter
@Entity
@Table(name = "schedules")
public class Schedule {

    @Id
    @GeneratedValue(
        strategy = GenerationType.AUTO
    )
    @Column(
        name = "id",
        unique = true,
        nullable = false
    )
    private Integer id;

    @Column(name = "code")
    private String code;

    @Column(name = "start")
    // @Type(type="org.jadira.usertype.dateandtime.joda.PersistentDateTime")
    private DateTime start;

    @Column(name = "finish")
    // @Type(type="org.jadira.usertype.dateandtime.joda.PersistentDateTime")
    private DateTime finish;

	public Integer getId() {
		return id;
	}

	public void setId(Integer id) {
		this.id = id;
	}

	public String getCode() {
		return code;
	}

	public void setCode(String code) {
		this.code = code;
	}

	public DateTime getStart() {
		return start;
	}

	public void setStart(DateTime start) {
		this.start = start;
	}

	public DateTime getFinish() {
		return finish;
	}

	public void setFinish(DateTime finish) {
		this.finish = finish;
	}
    
    
}
